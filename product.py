# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    active_ingredients_names = fields.Function(
        fields.Text('Active Ingredients Names', translate=True),
        'get_active_ingredients_names')

    def get_active_ingredients_names(self, name=None):
        return (self.composition.active_ingredients_names
            if self.composition else '')

    @fields.depends('composition',
        '_parent_composition.active_ingredients_names')
    def on_change_composition(self):
        super().on_change_composition()
        if self.composition:
            self.active_ingredients_names = \
                self.composition.active_ingredients_names
