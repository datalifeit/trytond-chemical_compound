# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Bool, Not, Or


class Ingredient(metaclass=PoolMeta):
    __name__ = 'product.composition.ingredient'

    active_ingredient = fields.Boolean('Active ingredient',
        states={'readonly': ~Eval('active')},
        depends=['active'])
    symbol = fields.Char('Symbol', size=3,
        states={
            'readonly': Or(~Eval('active'),
                Not(Bool(Eval('active_ingredient')))),
            'invisible': Not(Bool(Eval('active_ingredient')))
        },
        depends=['active', 'active_ingredient'])


class Composition(metaclass=PoolMeta):
    __name__ = 'product.composition'

    active_ingredients_names = fields.Function(
        fields.Text('Active Ingredients Names', translate=True),
        'get_active_ingredients_names')

    def get_active_ingredients_names(self, name=None, sep='\n'):
        return sep.join([ingredient.rec_name for ingredient in self.ingredients
            if ingredient.ingredient.active_ingredient])
