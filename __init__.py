# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import composition
from . import product


def register():
    Pool.register(
        composition.Ingredient,
        composition.Composition,
        product.Product,
        module='chemical_compound', type_='model')
