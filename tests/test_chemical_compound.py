# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
import doctest
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import suite as test_suite
from trytond.tests.test_tryton import doctest_teardown
from trytond.tests.test_tryton import doctest_checker
from trytond.pool import Pool


class ChemicalCompoundTestCase(ModuleTestCase):
    """Test Chemical Compound module"""
    module = 'chemical_compound'

    @with_transaction()
    def test_ingredient_crud(self):
        """Test composition CRUD"""

        pool = Pool()
        Ingredient = pool.get('product.composition.ingredient')
        Ingredient.create([
            {'name': 'ingredient1',
             'active_ingredient': True,
             'symbol': 'HG'}])
        ingredient, = Ingredient.search([])
        Ingredient.write([ingredient], {'symbol': 'FE'})
        Ingredient.delete([ingredient])


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            ChemicalCompoundTestCase))
    suite.addTests(doctest.DocFileSuite(
            'scenario_chemical_compound.rst',
            tearDown=doctest_teardown, encoding='utf-8',
            checker=doctest_checker,
            optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
